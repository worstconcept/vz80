#include "vz80.h"

#include "../obj_dir/Vz80_top_direct_n.h"

#include <godot_cpp/classes/global_constants.hpp>
#include <godot_cpp/classes/label.hpp>
#include <godot_cpp/core/class_db.hpp>
#include <godot_cpp/core/object.hpp>
#include <godot_cpp/variant/utility_functions.hpp>
#include <memory>

using namespace godot;

void Vz80::_bind_methods() {
	ClassDB::bind_method(D_METHOD("tick"), &Vz80::tick);
	ClassDB::bind_method(D_METHOD("get_false"), &Vz80::get_false);

	ClassDB::bind_method(D_METHOD("set_CLK", "pin"), &Vz80::set_CLK);
	ClassDB::bind_method(D_METHOD("set_nWAIT", "pin"), &Vz80::set_nWAIT);
	ClassDB::bind_method(D_METHOD("set_nINT", "pin"), &Vz80::set_nINT);
	ClassDB::bind_method(D_METHOD("set_nNMI", "pin"), &Vz80::set_nNMI);
	ClassDB::bind_method(D_METHOD("set_nRESET", "pin"), &Vz80::set_nRESET);
	ClassDB::bind_method(D_METHOD("set_nBUSRQ", "pin"), &Vz80::set_nBUSRQ);

	ClassDB::bind_method(D_METHOD("get_nM1"), &Vz80::get_nM1);
	ClassDB::bind_method(D_METHOD("get_nMREQ"), &Vz80::get_nMREQ);
	ClassDB::bind_method(D_METHOD("get_nIORQ"), &Vz80::get_nIORQ);
	ClassDB::bind_method(D_METHOD("get_nRD"), &Vz80::get_nRD);
	ClassDB::bind_method(D_METHOD("get_nWR"), &Vz80::get_nWR);
	ClassDB::bind_method(D_METHOD("get_nRFSH"), &Vz80::get_nRFSH);
	ClassDB::bind_method(D_METHOD("get_nHALT"), &Vz80::get_nHALT);
	ClassDB::bind_method(D_METHOD("get_nBUSACK"), &Vz80::get_nBUSACK);

	ClassDB::bind_method(D_METHOD("get_A"), &Vz80::get_A);
	ClassDB::bind_method(D_METHOD("get_D"), &Vz80::get_D);
	ClassDB::bind_method(D_METHOD("set_D", "bus"), &Vz80::set_D);


	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "CLK"), "set_CLK", "get_false");
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "nWAIT"), "set_nWAIT", "get_false");
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "nINT"), "set_nINT", "get_false");
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "nNMI"), "set_nNMI", "get_false");
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "nRESET"), "set_nRESET", "get_false");
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "nBUSRQ"), "set_nBUSRQ", "get_false");

	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "nM1"), "", "get_nM1");
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "nMREQ"), "", "get_nMREQ");
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "nIORQ"), "", "get_nIORQ");
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "nRD"), "", "get_nRD");
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "nWR"), "", "get_nWR");
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "nRFSH"), "", "get_nRFSH");
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "nHALT"), "", "get_nHALT");
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "nBUSACK"), "", "get_nBUSACK");

	ADD_PROPERTY(PropertyInfo(Variant::INT, "A"), "", "get_A");
	ADD_PROPERTY(PropertyInfo(Variant::INT, "D"), "set_D", "get_D");
}

Vz80::Vz80() {
	ctx = std::make_unique<VerilatedContext>();
	cpu = std::make_unique<Vz80_top_direct_n>(ctx.get(), "TOP");
}

Vz80::~Vz80() { cpu->final(); }

uint64_t Vz80::tick() {
	cpu->eval();
	ctx->timeInc(1);
	return ctx->time();
}

bool Vz80::get_false() const { return false; }

void Vz80::set_CLK(const bool &pin){ cpu->CLK = pin; }
void Vz80::set_nWAIT(const bool &pin){ cpu->nWAIT = pin; }
void Vz80::set_nINT(const bool &pin){ cpu->nINT = pin; }
void Vz80::set_nNMI(const bool &pin){ cpu->nNMI = pin; }
void Vz80::set_nRESET(const bool &pin){ cpu->nRESET = pin; }
void Vz80::set_nBUSRQ(const bool &pin){ cpu->nBUSRQ = pin; }

bool Vz80::get_nM1() const { return cpu->nM1; }
bool Vz80::get_nMREQ() const { return cpu->nMREQ; }
bool Vz80::get_nIORQ() const { return cpu->nIORQ; }
bool Vz80::get_nRD() const { return cpu->nRD; }
bool Vz80::get_nWR() const { return cpu->nWR; }
bool Vz80::get_nRFSH() const { return cpu->nRFSH; }
bool Vz80::get_nHALT() const { return cpu->nHALT; }
bool Vz80::get_nBUSACK() const { return cpu->nBUSACK; }

int Vz80::get_A() const { return cpu->A & 0xFFFF; }
int Vz80::get_D() const { return cpu->D & 0xFF; }
void Vz80::set_D(const int &bus) { cpu->D = bus & 0xFF; }
