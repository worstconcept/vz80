#pragma once

#include "../obj_dir/Vz80_top_direct_n.h"

#include <godot_cpp/classes/global_constants.hpp>
#include <godot_cpp/classes/ref_counted.hpp>
#include <godot_cpp/core/binder_common.hpp>
#include <memory>
#include <verilated.h>

using namespace godot;

class Vz80 : public RefCounted {
		GDCLASS(Vz80, RefCounted);

	private:
		std::unique_ptr<Vz80_top_direct_n> cpu;
		std::unique_ptr<VerilatedContext> ctx;

	protected:
		static void _bind_methods();

	public:
		Vz80();
		~Vz80();
		uint64_t tick();

		bool get_false() const;

    void set_CLK(const bool &pin);
    void set_nWAIT(const bool &pin);
    void set_nINT(const bool &pin);
    void set_nNMI(const bool &pin);
    void set_nRESET(const bool &pin);
    void set_nBUSRQ(const bool &pin);

		bool get_nM1() const;
    bool get_nMREQ() const;
    bool get_nIORQ() const;
    bool get_nRD() const;
    bool get_nWR() const;
    bool get_nRFSH() const;
    bool get_nHALT() const;
    bool get_nBUSACK() const;

    int get_A() const;
    int get_D() const;
    void set_D(const int &bus);
};
