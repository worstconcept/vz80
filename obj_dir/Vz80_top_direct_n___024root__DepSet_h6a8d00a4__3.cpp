// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vz80_top_direct_n.h for the primary calling header

#include "verilated.h"

#include "Vz80_top_direct_n___024root.h"

VL_INLINE_OPT void Vz80_top_direct_n___024root___combo__TOP__5(Vz80_top_direct_n___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vz80_top_direct_n__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vz80_top_direct_n___024root___combo__TOP__5\n"); );
    // Init
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla72pla55M1T2_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla72pla55M1T2_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla72pla55M1T3_1;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla72pla55M1T3_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla72pla55M2T1_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla72pla55M2T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_nuse_ixiypla72pla55M2T3_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_nuse_ixiypla72pla55M4T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla74npla55M1T1_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla74npla55M1T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla74npla55M1T3_1;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla74npla55M1T3_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla74npla55M1T4_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla74npla55M1T4_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_nuse_ixiypla74npla55M4T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla74pla55M1T3_1;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla74pla55M1T3_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla74pla55M2T1_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla74pla55M2T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_nuse_ixiypla74pla55M4T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla73npla55M1T1_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla73npla55M1T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla73npla55M1T3_1;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla73npla55M1T3_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla73npla55M1T4_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla73npla55M1T4_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_nuse_ixiypla73npla55M4T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla73pla55M1T3_1;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla73pla55M1T3_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla73pla55M2T1_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla73pla55M2T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_nuse_ixiypla73pla55M4T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla37npla28M1T1_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla37npla28M1T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla37npla28M2T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla37npla28M2T2_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla37npla28M3T1_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla37npla28M3T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla27npla34M1T1_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla27npla34M1T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla27npla34M1T2_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla27npla34M1T2_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla27npla34M1T3_1;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla27npla34M1T3_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla27npla34M2T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla37pla28M2T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla37pla28M2T2_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla37pla28M2T3_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla37pla28M2T3_5;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla37pla28M3T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla37pla28M3T1_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla27pla34M1T4nop4op5nop3_1;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla27pla34M1T4nop4op5nop3_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla27pla34M2T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla91pla21M1T2_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla21M1T2_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla91pla21M1T3_1;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla21M1T3_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla21M2T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla21M2T2_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla21M2T3_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla91pla21M3T1_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla21M3T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla91pla21M3T2_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla21M3T2_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla91pla21M4T1_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla91pla21M4T2_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla91pla21M4T3_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla91pla21M4T4_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla91pla20M1T2_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla20M1T2_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla91pla20M1T3_1;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla20M1T3_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla20M1T4_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla20M1T5_5;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla91pla20M2T1_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla20M2T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla91pla20M2T2_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla20M2T2_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla91pla20M2T3_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla20M2T3_5;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla20M3T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla91pla20M4T1_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla91pla20M4T2_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla91pla20M4T3_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla91pla20M4T4_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla29M2T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla29M2T2_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla29M2T3_6;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla29M3T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla29M3T2_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla29M3T3_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla29M3T3_9;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla43M1T3_1;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla43M1T3_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla43M2T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla43M2T2_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla43M2T3_6;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla43M3T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla43M3T2_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla43M3T3_5;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla43M3T3_10;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla47M1T3_1;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla47M1T3_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla47M2T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla47M2T2_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla47M3T2_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla47M3T3_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla47M3T4_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla47M3T5_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla47M3T5_8;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla48M1T3_1;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla48M1T3_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla48M2T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla48M2T2_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla48M3T2_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla48M3T3_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla48M3T4_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla48M3T5_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla48M3T5_8;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla6M1T4_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla6M1T4_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla26M1T3_1;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla26M1T3_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla26M1T4_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla26M1T5_5;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla26M2T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla26M2T2_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla26M3T2_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla26M3T3_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla26M3T4_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla26M3T5_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla26M3T5_8;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla24M2T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla24M2T2_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla24M2T3_6;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla24M3T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla24M3T2_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla24M3T3_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla24M3T4_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla24M3T4_5;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla24M4T1_6;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla24M4T2_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla24M4T2_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla24M4T3_5;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla24M4T3_6;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla24M5T1_6;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla24M5T2_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla24M5T2_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla24M5T3_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla42M1T3_1;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla42M1T3_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla42M2T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla42M2T2_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla42M2T3_6;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla42M3T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla42M3T2_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla42M3T3_6;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla42M3T4_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla42M3T4_5;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla42M4T1_6;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla42M4T2_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla42M4T2_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla42M4T3_5;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla42M4T3_6;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla42M5T1_6;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla42M5T2_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla42M5T2_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla42M5T3_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla35M2T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla35M2T1_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla35M2T2_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla35M2T2_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla35M2T3_6;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla35M3T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla35M3T1_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla35M3T2_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla35M3T2_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla35M3T3_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla35M3T3_9;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla45M1T3_1;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla45M1T3_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla45M2T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla45M2T1_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla45M2T2_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla45M2T2_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla45M2T3_6;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla45M3T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla45M3T1_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla45M3T2_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla45M3T2_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla45M3T3_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla45M3T3_9;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla46M2T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla46M2T1_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla46M2T2_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla46M2T2_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla46M2T3_6;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla46M3T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla46M3T1_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla46M3T2_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla46M3T2_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla46M3T3_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla46M3T3_9;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla56M1T3_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla56M1T5_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla56M1T5_5;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla56M2T1_6;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla56M2T2_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla56M2T2_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla56M2T3_5;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla56M2T3_6;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla56M3T1_6;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla56M3T2_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla56M3T2_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla56M3T3_6;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla56M4T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla56M4T3_6;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla56M5T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla56M5T3_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla56M5T3_9;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla49M1T3_1;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla49M1T3_2;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla49M2T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla49M2T2_4;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla49M3T1_3;
    CData/*0:0*/ z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla49M3T2_4;
    // Body
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 8U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                               >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | (((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                 & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                    >> 8U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                  >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                 & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 8U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 8U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                               >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 8U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 8U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 8U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_oe = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_oe) 
                                                 | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                      & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                         >> 8U)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                        >> 0x17U)) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla72pla55M1T2_2 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                           >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla72pla55M1T2_2)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla72pla55M1T2_3 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                           >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
           | (1U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla72pla55M1T2_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla72pla55M1T3_1 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                           >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla72pla55M1T3_1)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla72pla55M1T3_2 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                           >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla72pla55M1T3_2)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla72pla55M2T1_2 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                           >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
         | (2U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla72pla55M2T1_2)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla72pla55M2T1_3 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                           >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla72pla55M2T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 8U)) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                       >> 0x17U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 8U)) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                       >> 0x17U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_nuse_ixiypla72pla55M2T3_3 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                           >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_nuse_ixiypla72pla55M2T3_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                 & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                    >> 8U)) 
                                                & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                   >> 0x17U)) 
                                               & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 8U)) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                       >> 0x17U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 8U)) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                       >> 0x17U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | (((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                 & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                    >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                               >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_nuse_ixiypla72pla55M4T1_3 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                           >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_nuse_ixiypla72pla55M4T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 8U)) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                       >> 0x17U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 8U)) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                       >> 0x17U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                 & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                    >> 8U)) 
                                                & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                   >> 0x17U)) 
                                               & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 8U)) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                       >> 0x17U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 8U)) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                       >> 0x17U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | (((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                 & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                    >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                               >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 8U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla74npla55M1T1_2 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                               & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla74npla55M1T1_2))) 
                  & ((vlSelf->z80_top_direct_n__DOT__pla[3U] 
                      << 0x1cU) | (vlSelf->z80_top_direct_n__DOT__pla[3U] 
                                   >> 4U)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla74npla55M1T1_3 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                               & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
           | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla74npla55M1T1_3))) 
              & ((2U & ((~ (IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__rsel0)) 
                        << 1U)) | (IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__rsel0))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2u = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2u) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 0xaU)) 
                                                    & (~ 
                                                       (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                        >> 0x17U))) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_oe = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_oe) 
                                                 | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                      & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                         >> 0xaU)) 
                                                     & (~ 
                                                        (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                         >> 0x17U))) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                   >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                   >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                   >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla74npla55M1T3_1 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                               & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla74npla55M1T3_1)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla74npla55M1T3_2 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                               & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla74npla55M1T3_2)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                   >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                   >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                   >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                   >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                   >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                   >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                 & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                    >> 0xaU)) 
                                                & (~ 
                                                   (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x17U))) 
                                               & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla74npla55M1T4_3 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                               & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla74npla55M1T4_3))) 
                  & ((vlSelf->z80_top_direct_n__DOT__pla[3U] 
                      << 0x1cU) | (vlSelf->z80_top_direct_n__DOT__pla[3U] 
                                   >> 4U)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla74npla55M1T4_4 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                               & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
           | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla74npla55M1T4_4))) 
              & ((2U & ((~ (IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__rsel0)) 
                        << 1U)) | (IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__rsel0))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi) 
         | (((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
               & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                  >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                  >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                 & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
            & (~ (IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__rsel0))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo) 
         | (((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
               & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                  >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                  >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                 & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
            & (IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__rsel0)));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2u = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2u) 
                                                | (((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                      & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                         >> 0xaU)) 
                                                     & (~ 
                                                        (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                         >> 0x17U))) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
                                                   & (~ (IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__rsel0))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | (((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                      & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                         >> 0xaU)) 
                                                     & (~ 
                                                        (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                         >> 0x17U))) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
                                                   & (IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__rsel0)));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | (((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                 & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                    >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                    >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                   & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                   >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                   >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                   >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_nuse_ixiypla74npla55M4T1_3 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                               & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_nuse_ixiypla74npla55M4T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 0xaU)) 
                                                    & (~ 
                                                       (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                        >> 0x17U))) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 0xaU)) 
                                                    & (~ 
                                                       (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                        >> 0x17U))) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                   >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                   >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 0xaU)) 
                                                    & (~ 
                                                       (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                        >> 0x17U))) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 0xaU)) 
                                                    & (~ 
                                                       (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                        >> 0x17U))) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | (((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                 & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                    >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                    >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                   & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                   >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                   >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                   >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2u = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2u) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 0xaU)) 
                                                    & (~ 
                                                       (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                        >> 0x17U))) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_oe = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_oe) 
                                                 | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                      & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                         >> 0xaU)) 
                                                     & (~ 
                                                        (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                         >> 0x17U))) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                   >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                   >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                   >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                 & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                    >> 0xaU)) 
                                                & (~ 
                                                   (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x17U))) 
                                               & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla74pla55M1T3_1 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                             >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla74pla55M1T3_1)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla74pla55M1T3_2 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                             >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla74pla55M1T3_2)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla74pla55M2T1_2 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                             >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
         | (2U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla74pla55M2T1_2)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla74pla55M2T1_3 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                             >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla74pla55M2T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 0xaU)) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                       >> 0x17U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 0xaU)) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                       >> 0x17U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 0xaU)) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                       >> 0x17U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 0xaU)) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                       >> 0x17U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | (((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                 & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                    >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                               & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2u = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2u) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 0xaU)) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                       >> 0x17U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_oe = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_oe) 
                                                 | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                      & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                         >> 0xaU)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                        >> 0x17U)) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                 & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                    >> 0xaU)) 
                                                & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                   >> 0x17U)) 
                                               & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_nuse_ixiypla74pla55M4T1_3 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                             >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_nuse_ixiypla74pla55M4T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 0xaU)) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                       >> 0x17U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 0xaU)) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                       >> 0x17U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 0xaU)) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                       >> 0x17U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 0xaU)) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                       >> 0x17U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | (((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                 & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                    >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                               & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2u = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2u) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 0xaU)) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                       >> 0x17U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_oe = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_oe) 
                                                 | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                      & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                         >> 0xaU)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                        >> 0x17U)) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 0xaU)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                 & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                    >> 0xaU)) 
                                                & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                   >> 0x17U)) 
                                               & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla73npla55M1T1_2 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla73npla55M1T1_2))) 
                  & ((vlSelf->z80_top_direct_n__DOT__pla[3U] 
                      << 0x1cU) | (vlSelf->z80_top_direct_n__DOT__pla[3U] 
                                   >> 4U)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla73npla55M1T1_3 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
           | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla73npla55M1T1_3))) 
              & ((2U & ((~ (IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__rsel0)) 
                        << 1U)) | (IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__rsel0))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                               >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                               >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2u = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2u) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 9U)) 
                                                    & (~ 
                                                       (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                        >> 0x17U))) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_oe = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_oe) 
                                                 | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                      & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                         >> 9U)) 
                                                     & (~ 
                                                        (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                         >> 0x17U))) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                               >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla73npla55M1T3_1 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla73npla55M1T3_1)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla73npla55M1T3_2 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla73npla55M1T3_2)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                               >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                               >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                               >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                               >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                 & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                    >> 9U)) 
                                                & (~ 
                                                   (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x17U))) 
                                               & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla73npla55M1T4_3 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla73npla55M1T4_3))) 
                  & ((vlSelf->z80_top_direct_n__DOT__pla[3U] 
                      << 0x1cU) | (vlSelf->z80_top_direct_n__DOT__pla[3U] 
                                   >> 4U)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla73npla55M1T4_4 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
           | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla73npla55M1T4_4))) 
              & ((2U & ((~ (IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__rsel0)) 
                        << 1U)) | (IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__rsel0))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi) 
         | (((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
               & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                  >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                               & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
            & (~ (IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__rsel0))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo) 
         | (((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
               & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                  >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                               & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
            & (IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__rsel0)));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2u = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2u) 
                                                | (((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                      & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                         >> 9U)) 
                                                     & (~ 
                                                        (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                         >> 0x17U))) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
                                                   & (~ (IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__rsel0))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | (((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                      & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                         >> 9U)) 
                                                     & (~ 
                                                        (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                         >> 0x17U))) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
                                                   & (IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__rsel0)));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | (((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                 & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                    >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                  >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                 & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                               >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                               >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_nuse_ixiypla73npla55M4T1_3 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_nuse_ixiypla73npla55M4T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 9U)) 
                                                    & (~ 
                                                       (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                        >> 0x17U))) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 9U)) 
                                                    & (~ 
                                                       (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                        >> 0x17U))) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                               >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                               >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 9U)) 
                                                    & (~ 
                                                       (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                        >> 0x17U))) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 9U)) 
                                                    & (~ 
                                                       (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                        >> 0x17U))) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                               >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                               >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | (((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                 & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                    >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                  >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                 & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                               >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2u = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2u) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 9U)) 
                                                    & (~ 
                                                       (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                        >> 0x17U))) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_oe = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_oe) 
                                                 | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                      & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                         >> 9U)) 
                                                     & (~ 
                                                        (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                         >> 0x17U))) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                               >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                              & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 0x17U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                 & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                    >> 9U)) 
                                                & (~ 
                                                   (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x17U))) 
                                               & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla73pla55M1T3_1 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                           >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla73pla55M1T3_1)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla73pla55M1T3_2 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                           >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla73pla55M1T3_2)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla73pla55M2T1_2 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                           >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
         | (2U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_nuse_ixiypla73pla55M2T1_2)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla73pla55M2T1_3 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                           >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_nuse_ixiypla73pla55M2T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 9U)) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                       >> 0x17U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 9U)) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                       >> 0x17U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 9U)) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                       >> 0x17U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 9U)) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                       >> 0x17U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | (((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                 & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                    >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                               >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2u = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2u) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 9U)) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                       >> 0x17U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_oe = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_oe) 
                                                 | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                      & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                         >> 9U)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                        >> 0x17U)) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                 & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                    >> 9U)) 
                                                & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                   >> 0x17U)) 
                                               & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_nuse_ixiypla73pla55M4T1_3 
        = ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
             & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                           >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_nuse_ixiypla73pla55M4T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 9U)) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                       >> 0x17U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 9U)) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                       >> 0x17U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 9U)) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                       >> 0x17U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 9U)) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                       >> 0x17U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | (((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                 & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                    >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                               >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2u = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2u) 
                                                | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                        >> 9U)) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                       >> 0x17U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_oe = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_oe) 
                                                 | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                      & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                         >> 9U)) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                        >> 0x17U)) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe) 
         | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
              & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg) 
           | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                   >> 9U)) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 0x17U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | ((((~ (IData)(vlSelf->z80_top_direct_n__DOT__use_ixiy)) 
                                                 & (vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                    >> 9U)) 
                                                & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                   >> 0x17U)) 
                                               & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla37npla28M1T1_2 
        = (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 5U) & (~ (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x1cU))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla37npla28M1T1_2)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla37npla28M1T1_3 
        = (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 5U) & (~ (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x1cU))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
           | (2U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla37npla28M1T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
              >> 5U) & (~ (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x1cU))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
              >> 5U) & (~ (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x1cU))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                     >> 5U) 
                                                    & (~ 
                                                       (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                        >> 0x1cU))) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                     >> 5U) 
                                                    & (~ 
                                                       (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                        >> 0x1cU))) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
              >> 5U) & (~ (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x1cU))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 5U) & (~ (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x1cU))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
              >> 5U) & (~ (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x1cU))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla37npla28M2T1_3 
        = (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 5U) & (~ (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x1cU))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla37npla28M2T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                     >> 5U) 
                                                    & (~ 
                                                       (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                        >> 0x1cU))) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
              >> 5U) & (~ (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x1cU))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla37npla28M2T2_4 
        = (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 5U) & (~ (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x1cU))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla37npla28M2T2_4)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla37npla28M3T1_2 
        = (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 5U) & (~ (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x1cU))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla37npla28M3T1_2)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla37npla28M3T1_3 
        = (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 5U) & (~ (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x1cU))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
           | (2U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla37npla28M3T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                     >> 5U) 
                                                    & (~ 
                                                       (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                        >> 0x1cU))) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                     >> 5U) 
                                                    & (~ 
                                                       (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                        >> 0x1cU))) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
              >> 5U) & (~ (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x1cU))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
              >> 5U) & (~ (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x1cU))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                     >> 5U) 
                                                    & (~ 
                                                       (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                        >> 0x1cU))) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
              >> 5U) & (~ (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x1cU))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                 >> 5U) 
                                                & (~ 
                                                   (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 0x1cU))) 
                                               & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla27npla34M1T1_2 
        = (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                             >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla27npla34M1T1_2))) 
                  & ((vlSelf->z80_top_direct_n__DOT__pla[3U] 
                      << 0x19U) | (vlSelf->z80_top_direct_n__DOT__pla[3U] 
                                   >> 7U)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla27npla34M1T1_3 
        = (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                             >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
           | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla27npla34M1T1_3))) 
              & ((2U & ((~ (IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__rsel3)) 
                        << 1U)) | (IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__rsel3))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
              >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
              >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                     >> 0x1bU) 
                                                    & (~ 
                                                       (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                        >> 2U))) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                     >> 0x1bU) 
                                                    & (~ 
                                                       (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                        >> 2U))) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
              >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
              >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
              >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla27npla34M1T2_2 
        = (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                             >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla27npla34M1T2_2)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla27npla34M1T2_3 
        = (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                             >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
           | (1U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla27npla34M1T2_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
              >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
              >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla27npla34M1T3_1 
        = (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                             >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla27npla34M1T3_1)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla27npla34M1T3_2 
        = (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                             >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla27npla34M1T3_2)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
              >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
              >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | ((((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                 >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla27npla34M2T1_3 
        = (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                             >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla27npla34M2T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                     >> 0x1bU) 
                                                    & (~ 
                                                       (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                        >> 2U))) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                     >> 0x1bU) 
                                                    & (~ 
                                                       (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                        >> 2U))) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                 >> 0x1bU) 
                                                & (~ 
                                                   (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 2U))) 
                                               & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                     >> 0x1bU) 
                                                    & (~ 
                                                       (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                        >> 2U))) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                     >> 0x1bU) 
                                                    & (~ 
                                                       (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                        >> 2U))) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
              >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
              >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | ((((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                 >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                 >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
              >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1bU) & (~ (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                >> 2U))) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 5U) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x1cU)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
              >> 5U) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                        >> 0x1cU)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla37pla28M2T1_3 
        = (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 5U) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                       >> 0x1cU)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                     & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla37pla28M2T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                     >> 5U) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                       >> 0x1cU)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
              >> 5U) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                        >> 0x1cU)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla37pla28M2T2_4 
        = (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 5U) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                       >> 0x1cU)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                     & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla37pla28M2T2_4)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla37pla28M2T3_4 
        = (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 5U) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                       >> 0x1cU)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                     & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla37pla28M2T3_4)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla37pla28M2T3_5 
        = (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 5U) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                       >> 0x1cU)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                     & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
           | (2U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla37pla28M2T3_5)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                     >> 5U) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                       >> 0x1cU)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                     >> 5U) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                       >> 0x1cU)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
              >> 5U) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                        >> 0x1cU)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                     >> 5U) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                       >> 0x1cU)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
              >> 5U) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                        >> 0x1cU)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla37pla28M3T1_3 
        = (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 5U) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                       >> 0x1cU)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                     & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla37pla28M3T1_3)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla37pla28M3T1_4 
        = (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 5U) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                       >> 0x1cU)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                     & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
           | (2U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla37pla28M3T1_4)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
              >> 5U) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                        >> 0x1cU)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2u = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2u) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                     >> 5U) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                       >> 0x1cU)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                 >> 5U) 
                                                & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                   >> 0x1cU)) 
                                               & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                             >> 2U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_zero_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_zero_oe) 
           | ((((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                 >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                              >> 2U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
              & (IData)((0x180U == (0x1c0U & vlSelf->z80_top_direct_n__DOT__pla[3U])))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla27pla34M1T4nop4op5nop3_1 
        = ((((vlSelf->z80_top_direct_n__DOT__pla[0U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                           >> 2U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
           & (~ (IData)((0x180U == (0x1c0U & vlSelf->z80_top_direct_n__DOT__pla[3U])))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla27pla34M1T4nop4op5nop3_1))) 
                  & ((vlSelf->z80_top_direct_n__DOT__pla[3U] 
                      << 0x19U) | (vlSelf->z80_top_direct_n__DOT__pla[3U] 
                                   >> 7U)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla27pla34M1T4nop4op5nop3_2 
        = ((((vlSelf->z80_top_direct_n__DOT__pla[0U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                           >> 2U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
           & (~ (IData)((0x180U == (0x1c0U & vlSelf->z80_top_direct_n__DOT__pla[3U])))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
           | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla27pla34M1T4nop4op5nop3_2))) 
              & ((2U & ((~ (IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__rsel3)) 
                        << 1U)) | (IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__rsel3))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi) 
         | ((((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 2U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
            & (~ (IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__rsel3))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo) 
         | ((((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                            >> 2U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
            & (IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__rsel3)));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2u = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2u) 
                                                | ((((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                      >> 0x1bU) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                        >> 2U)) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
                                                   & (~ (IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__rsel3))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                      >> 0x1bU) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                        >> 2U)) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
                                                   & (IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__rsel3)));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla27pla34M2T1_3 
        = (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                          >> 2U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                     & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla27pla34M2T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                     >> 0x1bU) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                       >> 2U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                     >> 0x1bU) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                       >> 2U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                 >> 0x1bU) 
                                                & (vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                   >> 2U)) 
                                               & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_oe = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_oe) 
                                                 | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                      >> 0x1bU) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                        >> 0x15U)) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla91pla21M1T2_2 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla91pla21M1T2_2)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla21M1T2_3 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
           | (1U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla21M1T2_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla91pla21M1T3_1 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla91pla21M1T3_1)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla21M1T3_2 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla21M1T3_2)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | ((((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                              >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla21M2T1_3 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla21M2T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                     >> 0x1bU) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                       >> 0x15U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                     >> 0x1bU) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                       >> 0x15U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla21M2T2_3 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
           | (2U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla21M2T2_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | ((((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                              >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_zero 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_zero) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | ((((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                              >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low)));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl) 
           | ((((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                              >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low)));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf) 
           | ((((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                              >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla21M2T3_4 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
           | (2U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla21M2T3_4)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_oe = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_oe) 
                                                 | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                      >> 0x1bU) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                        >> 0x15U)) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | ((((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                              >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low)));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl) 
           | ((((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                              >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low)));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf) 
           | ((((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                              >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                     >> 0x1bU) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                       >> 0x15U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                     >> 0x1bU) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                       >> 0x15U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | ((((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                              >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla91pla21M3T1_2 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
         | (2U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla91pla21M3T1_2)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla21M3T1_3 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla21M3T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                     >> 0x1bU) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                       >> 0x15U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                     >> 0x1bU) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                       >> 0x15U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla91pla21M3T2_3 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
         | (2U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla91pla21M3T2_3)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla21M3T2_4 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla21M3T2_4)))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | ((((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                  >> 0x1bU) 
                                                 & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 0x15U)) 
                                                & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                   & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
                                               & ((IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__nonRep) 
                                                  | (IData)(vlSelf->z80_top_direct_n__DOT__alu_flags___DOT__SYNTHESIZED_WIRE_39))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla91pla21M4T1_2 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla91pla21M4T1_2)))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                     >> 0x1bU) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                       >> 0x15U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla91pla21M4T2_3 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla91pla21M4T2_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla91pla21M4T3_2 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla91pla21M4T3_2)))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                     >> 0x1bU) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                       >> 0x15U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla91pla21M4T4_3 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x15U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla91pla21M4T4_3)))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                 >> 0x1bU) 
                                                & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                   >> 0x15U)) 
                                               & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_oe = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_oe) 
                                                 | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                      >> 0x1bU) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                        >> 0x14U)) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla91pla20M1T2_2 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla91pla20M1T2_2)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla20M1T2_3 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
           | (1U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla20M1T2_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla91pla20M1T3_1 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla91pla20M1T3_1)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla20M1T3_2 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla20M1T3_2)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | ((((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                              >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla20M1T4_3 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
           | (2U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla20M1T4_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | ((((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                              >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_zero 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_zero) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | ((((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                              >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low)));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl) 
           | ((((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                              >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low)));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf) 
           | ((((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                              >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla20M1T5_5 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
           | (2U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla20M1T5_5)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_oe = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_oe) 
                                                 | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                      >> 0x1bU) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                        >> 0x14U)) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | ((((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                              >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low)));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl) 
           | ((((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                              >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low)));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf) 
           | ((((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                              >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla91pla20M2T1_2 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
         | (2U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla91pla20M2T1_2)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla20M2T1_3 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla20M2T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                     >> 0x1bU) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                       >> 0x14U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                     >> 0x1bU) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                       >> 0x14U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla91pla20M2T2_3 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
         | (2U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla91pla20M2T2_3)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla20M2T2_4 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla20M2T2_4)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla91pla20M2T3_4 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
         | (2U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla91pla20M2T3_4)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla20M2T3_5 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
           | (1U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla20M2T3_5)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                     >> 0x1bU) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                       >> 0x14U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | ((((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                              >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla20M3T1_3 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla91pla20M3T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                     >> 0x1bU) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                       >> 0x14U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                     >> 0x1bU) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                       >> 0x14U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                     >> 0x1bU) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                       >> 0x14U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                     >> 0x1bU) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                       >> 0x14U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | ((((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                              >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | ((((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                              >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low)));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl) 
           | ((((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                              >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low)));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf) 
           | ((((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                              >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_oe = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_oe) 
                                                 | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                      >> 0x1bU) 
                                                     & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                        >> 0x14U)) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                             >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | ((((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                              >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low)));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl) 
           | ((((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                              >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low)));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf) 
           | ((((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                 >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                              >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | ((((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                  >> 0x1bU) 
                                                 & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 0x14U)) 
                                                & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                   & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
                                               & ((IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__nonRep) 
                                                  | (IData)(vlSelf->z80_top_direct_n__DOT__alu_flags___DOT__SYNTHESIZED_WIRE_39))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla91pla20M4T1_2 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla91pla20M4T1_2)))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                     >> 0x1bU) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                       >> 0x14U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla91pla20M4T2_3 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla91pla20M4T2_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla91pla20M4T3_2 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla91pla20M4T3_2)))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                     >> 0x1bU) 
                                                    & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                       >> 0x14U)) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
              >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                           >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla91pla20M4T4_3 
        = (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
             >> 0x1bU) & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                          >> 0x14U)) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla91pla20M4T4_3)))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | (((vlSelf->z80_top_direct_n__DOT__pla[2U] 
                                                 >> 0x1bU) 
                                                & (vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                   >> 0x14U)) 
                                               & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x1dU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1dU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla29M2T1_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x1dU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla29M2T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 0x1dU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1dU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla29M2T2_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x1dU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla29M2T2_4)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_lo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_lo) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x1dU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1dU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla29M2T3_6 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x1dU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla29M2T3_6))) 
              & (1U | (2U & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1dU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 0x1dU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 0x1dU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1dU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1dU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla29M3T1_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x1dU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla29M3T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 0x1dU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1dU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla29M3T2_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x1dU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla29M3T2_4)))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                >> 0x1dU) 
                                               & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1dU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla29M3T3_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x1dU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla29M3T3_4)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 0x1dU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 0x1dU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_hi 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_hi) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x1dU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1dU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla29M3T3_9 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x1dU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla29M3T3_9))) 
              & (2U | (1U & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1dU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 0x1dU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 0x1dU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1dU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla43M1T3_1 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xbU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla43M1T3_1)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla43M1T3_2 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xbU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla43M1T3_2)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xbU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xbU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0xbU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xbU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xbU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xbU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xbU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xbU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xbU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xbU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla43M2T1_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xbU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla43M2T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xbU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xbU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla43M2T2_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xbU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla43M2T2_4)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_lo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_lo) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xbU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xbU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla43M2T3_6 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xbU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla43M2T3_6))) 
              & (1U | (2U & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xbU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xbU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xbU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xbU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xbU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla43M3T1_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xbU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla43M3T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xbU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xbU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla43M3T2_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xbU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla43M3T2_4)))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                >> 0xbU) 
                                               & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
              >> 0xbU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
            & (IData)(vlSelf->z80_top_direct_n__DOT__flags_cond_true)));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla43M3T3_5 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xbU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla43M3T3_5))) 
                    & (- (IData)((IData)(vlSelf->z80_top_direct_n__DOT__flags_cond_true))))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                     >> 0xbU) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
                                                   & (IData)(vlSelf->z80_top_direct_n__DOT__flags_cond_true)));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xbU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_hi 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_hi) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0xbU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__flags_cond_true)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
              >> 0xbU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
            & (IData)(vlSelf->z80_top_direct_n__DOT__flags_cond_true)));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla43M3T3_10 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xbU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla43M3T3_10))) 
              & (2U | (1U & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xbU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xbU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xbU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xbU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla47M1T3_1 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla47M1T3_1)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla47M1T3_2 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla47M1T3_2)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla47M2T1_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla47M2T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xfU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla47M2T2_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla47M2T2_4)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xfU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xfU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla47M3T2_2 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | (1U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla47M3T2_2)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xfU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low)));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low)));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_lo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_lo) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla47M3T3_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla47M3T3_3))) 
              & (1U | (2U & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2u = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2u) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xfU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_oe = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_oe) 
                                                 | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                     >> 0xfU) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low)));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low)));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla47M3T4_2 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | (2U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla47M3T4_2)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_zero 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_zero) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__alu_flags___DOT__DFFE_inst_latch_sf)));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                >> 0xfU) 
                                               & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla47M3T5_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla47M3T5_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xfU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xfU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_hi 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_hi) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla47M3T5_8 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla47M3T5_8))) 
              & (2U | (1U & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_oe = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_oe) 
                                                 | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                     >> 0xfU) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0xfU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__alu_flags___DOT__DFFE_inst_latch_sf)));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla48M1T3_1 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla48M1T3_1)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla48M1T3_2 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla48M1T3_2)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla48M2T1_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla48M2T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x10U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla48M2T2_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla48M2T2_4)))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                 >> 0x10U) 
                                                & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                   & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
                                               & (~ (IData)(vlSelf->z80_top_direct_n__DOT__flags_cond_true))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x10U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x10U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla48M3T2_2 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | (1U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla48M3T2_2)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x10U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low)));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low)));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_lo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_lo) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla48M3T3_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla48M3T3_3))) 
              & (1U | (2U & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2u = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2u) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x10U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_oe = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_oe) 
                                                 | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                     >> 0x10U) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low)));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low)));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla48M3T4_2 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | (2U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla48M3T4_2)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_zero 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_zero) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__alu_flags___DOT__DFFE_inst_latch_sf)));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                >> 0x10U) 
                                               & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla48M3T5_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla48M3T5_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x10U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x10U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_hi 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_hi) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla48M3T5_8 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla48M3T5_8))) 
              & (2U | (1U & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_oe = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_oe) 
                                                 | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                     >> 0x10U) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0x10U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__alu_flags___DOT__DFFE_inst_latch_sf)));
    vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 6U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                >> 6U) 
                                               & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla6M1T4_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 6U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
         | (2U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla6M1T4_3)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla6M1T4_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 6U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla6M1T4_4)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 6U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 6U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla26M1T3_1 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla26M1T3_1)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla26M1T3_2 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla26M1T3_2)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla26M1T4_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
           | (2U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla26M1T4_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_zero 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_zero) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low)));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low)));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla26M1T5_5 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
           | (2U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla26M1T5_5)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_oe = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_oe) 
                                                 | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                     >> 0x1aU) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low)));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low)));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla26M2T1_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla26M2T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 0x1aU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla26M2T2_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla26M2T2_4)))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                 >> 0x1aU) 
                                                & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                   & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
                                               & (IData)(vlSelf->z80_top_direct_n__DOT__alu_flags___DOT__SYNTHESIZED_WIRE_39)));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 0x1aU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 0x1aU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla26M3T2_2 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | (1U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla26M3T2_2)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 0x1aU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low)));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low)));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_lo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_lo) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla26M3T3_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla26M3T3_3))) 
              & (1U | (2U & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2u = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2u) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 0x1aU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_oe = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_oe) 
                                                 | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                     >> 0x1aU) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_set) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low)));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_cf_cpl) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low)));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla26M3T4_2 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | (2U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla26M3T4_2)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_zero 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_zero) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__alu_flags___DOT__DFFE_inst_latch_sf)));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                >> 0x1aU) 
                                               & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla26M3T5_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla26M3T5_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 0x1aU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 0x1aU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_hi 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_hi) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla26M3T5_8 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla26M3T5_8))) 
              & (2U | (1U & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_alu = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_alu) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_oe = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_oe) 
                                                 | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                     >> 0x1aU) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_res_oe) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_high) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_core_hf) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op_low))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_sel_op2_neg) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                >> 0x1aU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__alu_flags___DOT__DFFE_inst_latch_sf)));
    vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla24M2T1_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla24M2T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla24M2T2_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla24M2T2_4)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_lo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_lo) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla24M2T3_6 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla24M2T3_6))) 
              & (1U | (2U & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla24M3T1_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla24M3T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla24M3T2_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla24M3T2_4)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_hi 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_hi) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
               >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla24M3T3_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla24M3T3_4))) 
              & (2U | (1U & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla24M3T4_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla24M3T4_4)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla24M3T4_5 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla24M3T4_5)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla24M4T1_6 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | (2U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla24M4T1_6)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2u = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2u) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla24M4T2_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla24M4T2_3)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla24M4T2_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla24M4T2_4)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla24M4T3_5 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla24M4T3_5)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla24M4T3_6 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla24M4T3_6)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla24M5T1_6 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | (1U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla24M5T1_6)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla24M5T2_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla24M5T2_3)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla24M5T2_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla24M5T2_4)))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                >> 0x18U) 
                                               & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla24M5T3_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla24M5T3_4)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[0U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla42M1T3_1 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla42M1T3_1)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla42M1T3_2 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla42M1T3_2)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla42M2T1_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla42M2T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xaU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla42M2T2_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla42M2T2_4)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_lo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_lo) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla42M2T3_6 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla42M2T3_6))) 
              & (1U | (2U & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xaU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xaU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla42M3T1_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla42M3T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xaU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla42M3T2_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla42M3T2_4)))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                 >> 0xaU) 
                                                & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                   & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
                                               & (~ (IData)(vlSelf->z80_top_direct_n__DOT__flags_cond_true))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_hi 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_hi) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (IData)(vlSelf->z80_top_direct_n__DOT__flags_cond_true)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
              >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
            & (IData)(vlSelf->z80_top_direct_n__DOT__flags_cond_true)));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla42M3T3_6 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla42M3T3_6))) 
              & (2U | (1U & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xaU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xaU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla42M3T4_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla42M3T4_4)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla42M3T4_5 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla42M3T4_5)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xaU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xaU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla42M4T1_6 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | (2U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla42M4T1_6)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2u = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2u) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xaU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla42M4T2_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla42M4T2_3)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla42M4T2_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla42M4T2_4)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla42M4T3_5 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla42M4T3_5)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla42M4T3_6 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla42M4T3_6)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xaU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xaU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla42M5T1_6 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | (1U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla42M5T1_6)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla42M5T2_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla42M5T2_3)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla42M5T2_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla42M5T2_4)))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                >> 0xaU) 
                                               & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla42M5T3_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xaU) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla42M5T3_4)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xaU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xaU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 3U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla35M2T1_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 3U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla35M2T1_3)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla35M2T1_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 3U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla35M2T1_4)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 3U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 3U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla35M2T2_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 3U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla35M2T2_3)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla35M2T2_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 3U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla35M2T2_4)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_lo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_lo) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 3U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 3U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla35M2T3_6 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 3U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla35M2T3_6))) 
              & (1U | (2U & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 3U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 3U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 3U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 3U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla35M3T1_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 3U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla35M3T1_3)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla35M3T1_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 3U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla35M3T1_4)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 3U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 3U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla35M3T2_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 3U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla35M3T2_3)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla35M3T2_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 3U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla35M3T2_4)))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                >> 3U) 
                                               & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 3U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla35M3T3_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 3U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla35M3T3_4)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 3U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 3U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_hi 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_hi) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 3U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 3U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla35M3T3_9 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 3U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla35M3T3_9))) 
              & (2U | (1U & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 3U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 3U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 3U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 3U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla45M1T3_1 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xdU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla45M1T3_1)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla45M1T3_2 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xdU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla45M1T3_2)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xdU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xdU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0xdU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xdU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xdU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xdU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xdU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xdU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xdU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                 >> 0xdU) 
                                                & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                   & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))) 
                                               & (~ (IData)(vlSelf->z80_top_direct_n__DOT__flags_cond_true))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla45M2T1_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xdU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla45M2T1_3)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla45M2T1_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xdU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla45M2T1_4)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xdU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xdU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla45M2T2_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xdU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla45M2T2_3)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla45M2T2_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xdU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla45M2T2_4)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_lo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_lo) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xdU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xdU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla45M2T3_6 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xdU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla45M2T3_6))) 
              & (1U | (2U & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xdU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xdU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xdU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xdU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla45M3T1_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xdU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla45M3T1_3)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla45M3T1_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xdU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla45M3T1_4)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xdU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xdU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla45M3T2_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xdU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla45M3T2_3)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla45M3T2_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xdU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla45M3T2_4)))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                >> 0xdU) 
                                               & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xdU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla45M3T3_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xdU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla45M3T3_4)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xdU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xdU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_hi 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_hi) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xdU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xdU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla45M3T3_9 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xdU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla45M3T3_9))) 
              & (2U | (1U & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xdU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xdU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xdU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xdU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xeU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla46M2T1_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xeU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla46M2T1_3)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla46M2T1_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xeU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla46M2T1_4)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xeU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xeU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla46M2T2_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xeU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla46M2T2_3)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla46M2T2_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xeU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla46M2T2_4)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_lo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_lo) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xeU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xeU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla46M2T3_6 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xeU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla46M2T3_6))) 
              & (1U | (2U & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xeU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xeU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xeU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xeU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla46M3T1_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xeU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla46M3T1_3)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla46M3T1_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xeU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla46M3T1_4)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xeU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xeU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla46M3T2_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xeU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla46M3T2_3)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla46M3T2_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xeU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla46M3T2_4)))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                >> 0xeU) 
                                               & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xeU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla46M3T3_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xeU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla46M3T3_4)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xeU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xeU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_hi 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_hi) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0xeU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                           & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xeU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla46M3T3_9 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0xeU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                        & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla46M3T3_9))) 
              & (2U | (1U & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xeU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xeU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0xeU) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0xeU) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla56M1T3_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla56M1T3_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_oe = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_oe) 
                                                 | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                     >> 0x18U) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                     >> 0x18U) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
                                                   & (~ (IData)(vlSelf->z80_top_direct_n__DOT__interrupts___DOT__in_nmi_ALTERA_SYNTHESIZED))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_ff_oe = 
        (((vlSelf->z80_top_direct_n__DOT__pla[1U] >> 0x18U) 
          & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
         & ((IData)(vlSelf->z80_top_direct_n__DOT__interrupts___DOT__in_intr_ALTERA_SYNTHESIZED) 
            & (IData)(vlSelf->z80_top_direct_n__DOT__im1)));
    vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla56M1T5_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla56M1T5_4)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla56M1T5_5 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla56M1T5_5)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T5_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla56M2T1_6 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | (2U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla56M2T1_6)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2u = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2u) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla56M2T2_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla56M2T2_3)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla56M2T2_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla56M2T2_4)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla56M2T3_5 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla56M2T3_5)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla56M2T3_6 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla56M2T3_6)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla56M3T1_6 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | (1U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla56M3T1_6)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla56M3T2_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla56M3T2_3)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla56M3T2_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla56M3T2_4)))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                 >> 0x18U) 
                                                & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                   & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
                                               & (~ 
                                                  ((IData)(vlSelf->z80_top_direct_n__DOT__interrupts___DOT__in_intr_ALTERA_SYNTHESIZED) 
                                                   & (IData)(vlSelf->z80_top_direct_n__DOT__im2)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla56M3T3_6 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla56M3T3_6)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla56M4T1_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | (2U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla56M4T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2u = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2u) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_oe = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_oe) 
                                                 | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                     >> 0x18U) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_lo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_lo) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla56M4T3_6 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla56M4T3_6))) 
              & (1U | (2U & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M4_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla56M5T1_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | (2U & (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla56M5T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_lo) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2u = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2u) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_oe = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_oe) 
                                                 | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                     >> 0x18U) 
                                                    & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                       & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__setM1 = ((IData)(vlSelf->z80_top_direct_n__DOT__setM1) 
                                            | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                >> 0x18U) 
                                               & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                  & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla56M5T3_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla56M5T3_4)))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_4d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_4d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_hi 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_we_hi) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_wz) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla56M5T3_9 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
           | ((- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla56M5T3_9))) 
              & (2U | (1U & (IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_in_hi) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_2d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_2d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_sw_1d = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_sw_1d) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x18U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_bus_db_oe) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x18U) & ((IData)(vlSelf->z80_top_direct_n__DOT__M5) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla49M1T3_1 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x11U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel = 
        (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_sel) 
               | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_sel_pla49M1T3_1)))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla49M1T3_2 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x11U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_gp_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_gp_hilo_pla49M1T3_2)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_hi) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x11U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_out_lo) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x11U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_shift_oe) 
           | (((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                >> 0x11U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                             & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))) 
              & (~ (IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_bs_oe))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op2_sel_bus) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0x11U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_alu_op1_sel_bus) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0x11U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_sz_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0x11U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_xy_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0x11U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_flags_hf_we) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0x11U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
    vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__validPLA) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0x11U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M1_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T4_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x11U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla49M2T1_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x11U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla49M2T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x11U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x11U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla49M2T2_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x11U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M2_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla49M2T2_4)))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x11U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla49M3T1_3 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x11U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla49M3T1_3)))));
    vlSelf->z80_top_direct_n__DOT__ctl_al_we = ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_al_we) 
                                                | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
                                                    >> 0x11U) 
                                                   & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                                                      & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__execute___DOT__ixy_d 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__ixy_d) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0x11U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T1_ff))));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc = 
        ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sel_pc) 
         | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
             >> 0x11U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                          & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla49M3T2_4 
        = ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
            >> 0x11U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                         & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff)));
    vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo 
        = (3U & ((IData)(vlSelf->z80_top_direct_n__DOT__ctl_reg_sys_hilo) 
                 | (- (IData)((IData)(z80_top_direct_n__DOT__execute___DOT__ctl_reg_sys_hilo_pla49M3T2_4)))));
    vlSelf->z80_top_direct_n__DOT__execute___DOT__ixy_d 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__ixy_d) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0x11U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T2_ff))));
    vlSelf->z80_top_direct_n__DOT__execute___DOT__ixy_d 
        = ((IData)(vlSelf->z80_top_direct_n__DOT__execute___DOT__ixy_d) 
           | ((vlSelf->z80_top_direct_n__DOT__pla[1U] 
               >> 0x11U) & ((IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_M3_ff) 
                            & (IData)(vlSelf->z80_top_direct_n__DOT__sequencer___DOT__DFFE_T3_ff))));
}
