// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Symbol table implementation internals

#include "Vz80_top_direct_n__Syms.h"
#include "Vz80_top_direct_n.h"
#include "Vz80_top_direct_n___024root.h"

// FUNCTIONS
Vz80_top_direct_n__Syms::~Vz80_top_direct_n__Syms()
{
}

Vz80_top_direct_n__Syms::Vz80_top_direct_n__Syms(VerilatedContext* contextp, const char* namep,Vz80_top_direct_n* modelp)
    : VerilatedSyms{contextp}
    // Setup internal state of the Syms class
    , __Vm_modelp{modelp}
    // Setup module instances
    , TOP{this, namep}
{
    // Configure time unit / time precision
    _vm_contextp__->timeunit(-6);
    _vm_contextp__->timeprecision(-15);
    // Setup each module's pointers to their submodules
    // Setup each module's pointer back to symbol table (for public functions)
    TOP.__Vconfigure(true);
}
