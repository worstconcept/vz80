// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Symbol table internal header
//
// Internal details; most calling programs do not need this header,
// unless using verilator public meta comments.

#ifndef VERILATED_VZ80_TOP_DIRECT_N__SYMS_H_
#define VERILATED_VZ80_TOP_DIRECT_N__SYMS_H_  // guard

#include "verilated.h"

// INCLUDE MODEL CLASS

#include "Vz80_top_direct_n.h"

// INCLUDE MODULE CLASSES
#include "Vz80_top_direct_n___024root.h"

// SYMS CLASS (contains all model state)
class Vz80_top_direct_n__Syms final : public VerilatedSyms {
  public:
    // INTERNAL STATE
    Vz80_top_direct_n* const __Vm_modelp;
    bool __Vm_didInit = false;

    // MODULE INSTANCE STATE
    Vz80_top_direct_n___024root    TOP;

    // CONSTRUCTORS
    Vz80_top_direct_n__Syms(VerilatedContext* contextp, const char* namep, Vz80_top_direct_n* modelp);
    ~Vz80_top_direct_n__Syms();

    // METHODS
    const char* name() { return TOP.name(); }
} VL_ATTR_ALIGNED(VL_CACHE_LINE_BYTES);

#endif  // guard
