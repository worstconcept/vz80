// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vz80_top_direct_n.h for the primary calling header

#include "verilated.h"

#include "Vz80_top_direct_n__Syms.h"
#include "Vz80_top_direct_n___024root.h"

void Vz80_top_direct_n___024root___ctor_var_reset(Vz80_top_direct_n___024root* vlSelf);

Vz80_top_direct_n___024root::Vz80_top_direct_n___024root(Vz80_top_direct_n__Syms* symsp, const char* name)
    : VerilatedModule{name}
    , vlSymsp{symsp}
 {
    // Reset structure values
    Vz80_top_direct_n___024root___ctor_var_reset(this);
}

void Vz80_top_direct_n___024root::__Vconfigure(bool first) {
    if (false && first) {}  // Prevent unused
}

Vz80_top_direct_n___024root::~Vz80_top_direct_n___024root() {
}
