// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Model implementation (design independent parts)

#include "Vz80_top_direct_n.h"
#include "Vz80_top_direct_n__Syms.h"

//============================================================
// Constructors

Vz80_top_direct_n::Vz80_top_direct_n(VerilatedContext* _vcontextp__, const char* _vcname__)
    : vlSymsp{new Vz80_top_direct_n__Syms(_vcontextp__, _vcname__, this)}
    , CLK{vlSymsp->TOP.CLK}
    , nM1{vlSymsp->TOP.nM1}
    , nMREQ{vlSymsp->TOP.nMREQ}
    , nIORQ{vlSymsp->TOP.nIORQ}
    , nRD{vlSymsp->TOP.nRD}
    , nWR{vlSymsp->TOP.nWR}
    , nRFSH{vlSymsp->TOP.nRFSH}
    , nHALT{vlSymsp->TOP.nHALT}
    , nBUSACK{vlSymsp->TOP.nBUSACK}
    , nWAIT{vlSymsp->TOP.nWAIT}
    , nINT{vlSymsp->TOP.nINT}
    , nNMI{vlSymsp->TOP.nNMI}
    , nRESET{vlSymsp->TOP.nRESET}
    , nBUSRQ{vlSymsp->TOP.nBUSRQ}
    , D{vlSymsp->TOP.D}
    , A{vlSymsp->TOP.A}
    , rootp{&(vlSymsp->TOP)}
{
}

Vz80_top_direct_n::Vz80_top_direct_n(const char* _vcname__)
    : Vz80_top_direct_n(nullptr, _vcname__)
{
}

//============================================================
// Destructor

Vz80_top_direct_n::~Vz80_top_direct_n() {
    delete vlSymsp;
}

//============================================================
// Evaluation loop

void Vz80_top_direct_n___024root___eval_initial(Vz80_top_direct_n___024root* vlSelf);
void Vz80_top_direct_n___024root___eval_settle(Vz80_top_direct_n___024root* vlSelf);
void Vz80_top_direct_n___024root___eval(Vz80_top_direct_n___024root* vlSelf);
QData Vz80_top_direct_n___024root___change_request(Vz80_top_direct_n___024root* vlSelf);
#ifdef VL_DEBUG
void Vz80_top_direct_n___024root___eval_debug_assertions(Vz80_top_direct_n___024root* vlSelf);
#endif  // VL_DEBUG
void Vz80_top_direct_n___024root___final(Vz80_top_direct_n___024root* vlSelf);

static void _eval_initial_loop(Vz80_top_direct_n__Syms* __restrict vlSymsp) {
    vlSymsp->__Vm_didInit = true;
    Vz80_top_direct_n___024root___eval_initial(&(vlSymsp->TOP));
    // Evaluate till stable
    int __VclockLoop = 0;
    QData __Vchange = 1;
    do {
        VL_DEBUG_IF(VL_DBG_MSGF("+ Initial loop\n"););
        Vz80_top_direct_n___024root___eval_settle(&(vlSymsp->TOP));
        Vz80_top_direct_n___024root___eval(&(vlSymsp->TOP));
        if (VL_UNLIKELY(++__VclockLoop > 100)) {
            // About to fail, so enable debug to see what's not settling.
            // Note you must run make with OPT=-DVL_DEBUG for debug prints.
            int __Vsaved_debug = Verilated::debug();
            Verilated::debug(1);
            __Vchange = Vz80_top_direct_n___024root___change_request(&(vlSymsp->TOP));
            Verilated::debug(__Vsaved_debug);
            VL_FATAL_MT("az80/z80_top_direct_n.v", 19, "",
                "Verilated model didn't DC converge\n"
                "- See https://verilator.org/warn/DIDNOTCONVERGE");
        } else {
            __Vchange = Vz80_top_direct_n___024root___change_request(&(vlSymsp->TOP));
        }
    } while (VL_UNLIKELY(__Vchange));
}

void Vz80_top_direct_n::eval_step() {
    VL_DEBUG_IF(VL_DBG_MSGF("+++++TOP Evaluate Vz80_top_direct_n::eval_step\n"); );
#ifdef VL_DEBUG
    // Debug assertions
    Vz80_top_direct_n___024root___eval_debug_assertions(&(vlSymsp->TOP));
#endif  // VL_DEBUG
    // Initialize
    if (VL_UNLIKELY(!vlSymsp->__Vm_didInit)) _eval_initial_loop(vlSymsp);
    // Evaluate till stable
    int __VclockLoop = 0;
    QData __Vchange = 1;
    do {
        VL_DEBUG_IF(VL_DBG_MSGF("+ Clock loop\n"););
        Vz80_top_direct_n___024root___eval(&(vlSymsp->TOP));
        if (VL_UNLIKELY(++__VclockLoop > 100)) {
            // About to fail, so enable debug to see what's not settling.
            // Note you must run make with OPT=-DVL_DEBUG for debug prints.
            int __Vsaved_debug = Verilated::debug();
            Verilated::debug(1);
            __Vchange = Vz80_top_direct_n___024root___change_request(&(vlSymsp->TOP));
            Verilated::debug(__Vsaved_debug);
            VL_FATAL_MT("az80/z80_top_direct_n.v", 19, "",
                "Verilated model didn't converge\n"
                "- See https://verilator.org/warn/DIDNOTCONVERGE");
        } else {
            __Vchange = Vz80_top_direct_n___024root___change_request(&(vlSymsp->TOP));
        }
    } while (VL_UNLIKELY(__Vchange));
    // Evaluate cleanup
}

//============================================================
// Utilities

VerilatedContext* Vz80_top_direct_n::contextp() const {
    return vlSymsp->_vm_contextp__;
}

const char* Vz80_top_direct_n::name() const {
    return vlSymsp->name();
}

//============================================================
// Invoke final blocks

VL_ATTR_COLD void Vz80_top_direct_n::final() {
    Vz80_top_direct_n___024root___final(&(vlSymsp->TOP));
}
